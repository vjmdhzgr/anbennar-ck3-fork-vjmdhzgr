k_lorent = {
	985.10.10 = {
		holder = 28	#Rewan the Thorn of the West
	}
	1014.1.4 = {
		holder = 31	#Kylian
	}
	1015.11.1 = {
		holder = 27	#Ruben, husband of Ioriel
	}
	1020.10.27 = {
		holder = 2	#Rean Siloriel
	}
}

d_redglades = {
	1000.1.1 = {
		liege = k_lorent
	}
	1018.1.1 = {
		holder = 15	#Ioriel
	}
}

d_lower_bloodwine = {
	1000.1.1 = {
		liege = k_lorent
		holder = 20000	#Thayen
	}
}
