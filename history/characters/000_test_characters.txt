﻿1 = {
	name = "Marion"
	dna = 1_marion_firstborn
	dynasty = dynasty_silmuna	#Silmuna
	religion = "cult_of_the_dame"
	culture = "damerian"
	mother = 13	#Auci
	father = 20 #Munas
	
	diplomacy = 13
	martial = 10
	stewardship = 4
	intrigue = 9
	learning = 2
	prowess = 13	#hes a good enough knight to fight sorcerer king or be selected
	trait = education_martial_3
	trait = reckless
	trait = gregarious
	trait = ambitious
	trait = impatient
	trait = beauty_good_2
	trait = half_elf

	
	1001.2.19 = {
		birth = yes
	}
	
	1002.1.1 = {
		give_nickname = nick_firstborn
	}

	1020.10.31 = {
		set_relation_friend = character:36	#Ottrac the Wexonard
	}

}

2 = {
	name = "Reán"
	dynasty = dynasty_siloriel	#Siloriel
	religion = "court_of_adean"
	culture = "lorentish"
	mother = 15	#Ioriel
	father = 27	#Ruben

	trait = beauty_good_1
	trait = half_elf

	1020.1.1 = {
		birth = yes
	}
}

3 = {
	name = "Aron"
	dynasty = 9	#Pearlman
	religion = "castanorian_pantheon"
	culture = "pearlsedger"
		
	diplomacy = 4
	martial = 12
	stewardship = 7
	intrigue = 2
	learning = 5

	trait = education_martial_4
	trait = organizer
	trait = patient
	trait = brave
	trait = lustful
	trait = giant
	trait = human
	
	980.4.13 = {
		birth = yes
	}
}


4 = {
	name = "Petrus"
	dynasty_house = house_vivin	#sil Vivin
	religion = "court_of_adean"
	culture = "roilsardi"
	father = 508

	trait = education_intrigue_2
	trait = vengeful
	trait = content
	trait = lustful
	trait = fecund
	trait = human
	
	1002.9.12 = {
		birth = yes
	}

	1016.1.19 = {
		add_spouse = 510	#Lilith of Sorncost
	}

}


5 = {
	name = "Caylen"
	dynasty_house = house_loop	#silna Loop
	religion = "house_of_minara"
	culture = "roilsardi"

	trait = human
	
	977.2.23 = {
		birth = yes
	}


}


6 = {
	name = "Gregoire"
	dynasty_house = house_roilsard	#sil Roilsard
	religion = "court_of_adean"
	culture = "roilsardi"
	father = 504
	
	trait = human

	980.1.1 = {
		birth = yes
	}
}

7 = {
	name = "Kalbard"
	dynasty = 12	#sil Deranne
	religion = "court_of_adean"
	culture = "derannic"

	diplomacy = 4
	martial = 13
	stewardship = 7
	intrigue = 4
	learning = 14

	trait = education_learning_2
	trait = temperate
	trait = stubborn
	trait = chaste
	trait = human
	
	960.10.3 = {
		birth = yes
	}
}

8 = {
	name = "Alcuin"
	dynasty = dynasty_sorncost	#sil Sorncost
	religion = "court_of_adean"
	culture = "sorncosti"
	father = 511

	diplomacy = 10
	martial = 4
	stewardship = 10
	intrigue = 9
	learning = 6

	trait = education_intrigue_4
	trait = arbitrary
	trait = paranoid
	trait = fickle
	trait = disfigured
	trait = human
	
	995.3.12 = {
		birth = yes
	}
}

9 = {
	name = "Crege"
	dna = 2_crege_dameris
	dynasty_house = house_dameris #Dameris
	religion = cult_of_the_dame
	culture = "carnetori"
	father = 24 #Marven Dameris
	
	diplomacy = 6
	martial = 10
	stewardship = 10
	intrigue = 4
	learning = 1
	prowess = 10 #Trained with Marion growing up, fought alongside him through Escann
	trait = education_stewardship_3
	trait = just
	trait = diligent
	trait = brave
	trait = physique_good_2
	trait = human
	
	998.4.28 = {
		birth = yes
	}
	
	1002.1.1 = {
		set_relation_best_friend = character:1
	}
	
	1018.3.2 = {
		add_spouse = 12
		set_relation_lover = character:12
	}
}

10 = {
	name = "Lucian"	#Lucian the Protector of Corvuria
	dynasty_house = house_vivin	#sil Vivin
	religion = "court_of_adean"
	culture = "roilsardi"
	father = 4	#Petrus, Count of Vivinmar

	trait = physique_good_2
	trait = fecund
	trait = bossy
	trait = human
	
	1017.3.5 = {
		birth = yes
	}


}

11 = {
	name = "Taliesin"
	dynasty_house = house_dameris #Dameris
	religion = cult_of_the_dame
	culture = "carnetori"
	
	trait = physique_good_2
	trait = human
	
	1019.2.14 = {
		birth = yes
	}
	
	father = 9
	mother = 12
}

12 = {
	name = "Alise"
	dna = 4_alise_loop
	dynasty_house = house_loop #Loop
	religion = house_of_minara
	culture = "roilsardi"
	female = yes
	
	trait = education_diplomacy_4
	trait = compassionate
	trait = gregarious
	trait = lustful
	trait = human
	
	1000.1.1 = {
		birth = yes
	}
	
	father = 5
}

13 = {
	name = "Auci"
	dna = 13_auci
	dynasty_house = house_dameris #Dameris
	religion = cult_of_the_dame
	culture = "old_damerian"
	female = yes
	father = 25 #Galien Dameris
	
	trait = education_learning_4
	trait = compassionate
	trait = trusting
	trait = content
	trait = beauty_good_2
	trait = human
	
	976.5.5 = {
		birth = yes
	}

	977.1.1 = {
		give_nickname = nick_eightborn
	}

	1021.10.3 = {
		death = "1021.10.3"
	}
}

15 = {
	name = "Ioriel"
	dna = 5_ioriel_redrose
	dynasty = dynasty_siloriel	#Siloriel
	religion = "court_of_adean"
	culture = "moon_elvish"
	female = yes
	
	trait = education_diplomacy_4
	trait = ambitious
	trait = patient
	trait = gregarious
	trait = shrewd
	trait = diplomat
	trait = beauty_good_3
	trait = elf
	
	938.2.19 = {
		birth = yes
	}
	
	1000.1.1 = {
		give_nickname = nick_redrose
	}

	1018.2.14 = {	#Valentines
		add_spouse = 27 #Rewan
	}
}

16 = {
	name = "Narawen"
	dna = 6_narawen_the_wanderer
	dynasty = dynasty_silnara #Silnara
	religion = "cult_of_the_dame"
	culture = "moon_elvish"
	female = yes
	
	diplomacy = 8
	martial = 9
	stewardship = 8
	intrigue = 8
	learning = 6
	prowess = 8
	trait = education_martial_2
	trait = fickle
	trait = brave
	trait = impatient
	trait = intellect_good_1
	trait = hunter_2
	trait = elf
	
	897.7.21 = {	#shes the second youngest silver family founder, Ioriel is the youngest
		birth = yes
	}
	
	1000.1.1 = {
		give_nickname = nick_the_wanderer
	}
}

17 = {
	name = "Eborian"
	dna = 7_eborian_goldwaters
	dynasty = dynasty_silebor #Silebor
	religion = "cult_of_the_dame"
	culture = "moon_elvish"
	
	diplomacy = 6
	martial = 7
	stewardship = 10
	intrigue = 8
	learning = 8
	prowess = 5
	trait = education_stewardship_4
	trait = greedy
	trait = fickle
	trait = ambitious
	trait = shrewd
	trait = avaricious
	trait = elf
	
	799.1.20 = {
		birth = yes
	}
	
	1000.1.1 = {
		effect = {
			give_nickname = nick_goldwaters
		}
	}
}
	
18 = {
	name = "Garion"
	dna = 8_garion_the_wise
	dynasty = dynasty_silgarion #Silgarion
	religion = "cult_of_the_dame"
	culture = "moon_elvish"
	
	diplomacy = 8
	martial = 6
	stewardship = 10
	intrigue = 7
	learning = 12
	prowess = 10
	trait = education_learning_4
	trait = content
	trait = patient
	trait = calm
	trait = intellect_good_3
	trait = scholar
	trait = one_legged
	trait = maimed
	trait = elf
	
	840.8.13 = {	#born on the month of Ysdhament, very magical. He survives til 1221 aka formation of Anbennar so he needs to be youngish
		birth = yes
	}
	
	1000.1.1 = {
		give_nickname = nick_the_wise
	}
}

19 = {
	name = "Istralania"
	dna = 9_istralania_warsinger
	dynasty = dynasty_silistra #Silistra
	religion = "cult_of_the_dame"
	culture = "moon_elvish"
	female = yes
	
	diplomacy = 4
	martial = 10
	stewardship = 6
	intrigue = 2
	learning = 5
	prowess = 15
	trait = education_martial_3
	trait = aggressive_attacker
	trait = stubborn
	trait = wrathful
	trait = brave
	trait = blademaster_2
	trait = physique_good_3
	trait = scarred

	trait = elf
	
	777.7.7 = {
		birth = yes
	}
	
	1000.1.1 = {
		give_nickname = nick_warsinger
	}

	1020.11.22 = {
		#add_matrilineal_spouse = 37	#Istralania Warsinger finally accepts Sideric's advances and she marries him after learning her priorities after Trialmount
		set_relation_lover = character:37
	}
}

20 = {
	name = "Munas"
	dna = 10_munas_moonsinger
	dynasty = dynasty_silmuna #Silmuna
	religion = "cult_of_the_dame"
	culture = "moon_elvish"
	
	diplomacy = 10
	martial = 8
	stewardship = 8
	intrigue = 4
	learning = 5
	prowess = 8
	trait = education_diplomacy_3
	trait = trusting
	trait = zealous
	trait = stubborn
	trait = beauty_good_2
	trait = strategist
	trait = elf
	
	828.1.1 = {
		birth = yes
	}
	
	990.1.1 = {
		give_nickname = nick_moonsinger
	}
	
	1000.1.1 = {
		add_spouse = 13
	}
	
	1013.10.3 = {
		death = "1013.10.3"
	}
}

21 = {
	name = "Urion"
	dna = 11_urion_starsworn
	dynasty = dynasty_silurion #Silurion
	religion = "court_of_adean"
	culture = "moon_elvish"
	
	diplomacy = 8
	martial = 8
	stewardship = 5
	intrigue = 2
	learning = 5
	prowess = 10
	trait = education_martial_3
	trait = education_martial_prowess_4
	trait = brave
	trait = chaste
	trait = zealous
	trait = gallant
	trait = blademaster_3
	trait = elf
	
	820.10.23 = {
		birth = yes
	}
	
	1000.1.1 = {
		give_nickname = nick_starsworn
	}

}

22 = {
	name = "Calasandur"
	dna = 12_calasandur_the_magnificent
	dynasty = dynasty_silcalas #Silcalas
	religion = "cult_of_the_dame"
	culture = "moon_elvish"
	
	diplomacy = 7
	martial = 5
	stewardship = 12
	intrigue = 5
	learning = 5
	prowess = 6
	trait = education_stewardship_4
	trait = greedy
	trait = diligent
	trait = fickle	#kind of an eccentric dude
	trait = architect
	trait = fecund
	trait = intellect_good_2
	trait = elf
	
	753.2.3 = {		#hes young but he's oooold
		birth = yes
	}
	
	1000.1.1 = {
		give_nickname = nick_the_magnificent
	}
}

23 = {
	name = "Jaher"
	dna = 13_jaher
	dynasty = 15 #Jaherzuir
	religion = "cult_of_the_dame"
	culture = "sun_elvish"
	
	diplomacy = 5
	martial = 10
	stewardship = 5
	intrigue = 8
	learning = 7
	prowess = 14
	trait = education_martial_4
	trait = ambitious
	trait = arrogant
	trait = impatient
	trait = intellect_good_1
	trait = physique_good_3
	trait = beauty_good_2
	trait = elf
	
	791.6.1 = {	#June 1st, aka the first day of our month Suren aka Sun's Start - almost as if hes the chosen one
		birth = yes
	}
	
}

24 = {
	name = "Marven"
	dynasty_house = house_dameris #Dameris
	religion = "cult_of_the_dame"
	culture = "carnetori"
	father = 25 #Galien Dameris
	
	trait = education_martial_2
	trait = reckless
	trait = impatient
	trait = arrogant
	trait = brave
	trait = physique_good_3
	trait = human
	
	980.2.4 = {
		birth = yes
	}
	
	998.2.1 = {
		death = "998.2.1"
	}
}

25 = {
	name = "Galien"
	dynasty_house = house_dameris #Dameris
	religion = "cult_of_the_dame"
	culture = "old_damerian"
	
	trait = education_learning_4
	trait = patient
	trait = zealous
	trait = forgiving
	trait = theologian
	trait = beauty_good_3
	trait = human
	
	930.6.8 = {
		birth = yes
	}
	
	980.12.1 = {
		death = "980.12.1"
	}
}

26 = {
	name = "Finnic"
	dna = 3_finnic_sil_vis
	dynasty = 11 #sil Vis
	religion = "small_temple"
	culture = "hillfoot_halfling"
	
	trait = education_intrigue_4
	trait = brave
	trait = deceitful
	trait = humble
	trait = intellect_good_2
	
	trait = halfling
	
	1001.4.1 = {
		birth = yes
	}
	
	1005.1.1 = {
		give_nickname = nick_shadowhand
	}
}

#27 - 40 - Jay
27 = {
	name = "Ruben"
	dynasty_house = house_lorentis
	religion = "court_of_adean"
	culture = "lorentish"
	father = 28 #King Rewan the Thorn of the West
	#mother = 6401 

	trait = trusting
	trait = diligent
	trait = ambitious
	trait = education_diplomacy_4

	trait = human

	1000.1.10 = {
		birth = yes
	}

	1000.2.14 = {	#Valentines
		add_matrilineal_spouse = 15 #Ioriel
	}

	1020.10.27 = {	#During Tourney of Grand Victory
		death = {
			death_reason = death_duel
			killer = 9 # Crege of Carneter
		}
	}
}

28 = {
	name = "Rewan"
	dynasty_house = house_lorentis
	religion = "court_of_adean"
	culture = "lorenti"
	#father = 28 
	mother = 29 	#Raisenda sina Loop

	trait = stubborn
	trait = patient
	trait = brave
	trait = education_martial_3

	trait = human

	969.6.21 = {
		birth = yes
	}

	990.1.1 = {
		give_nickname = nick_the_thorn_of_the_west
	}

	1014.1.4 = {
		death = {
			death_reason = death_ill
		}
	}
}

29 = {
	name = "Raisenda"
	dynasty_house = house_loop
	religion = "court_of_adean"
	culture = "roilsardi"
	female = yes
	#father = 28 
	#mother = 6401 

	trait = cynical
	trait = generous
	trait = patient
	trait = education_diplomacy_2

	trait = human

	948.4.23 = {
		birth = yes
	}
	1005.1.3 = {
		death = {
			death_reason = death_natural_causes
		}
	}
}

30 = {
	name = "Rewan"
	dynasty_house = house_lorentis
	religion = "court_of_adean"
	culture = "lorenti"
	father = 28 #King Rewan the Thorn of the West
	#mother = 6401 

	trait = human


	990.12.5 = {
		birth = yes
	}

	991.1.13 = {
		give_nickname = nick_the_younger
	}

	1001.1.12 = {	
		death = {
			death_reason = death_execution
		}
	}
}

31 = {
	name = "Kylian"
	dynasty_house = house_lorentis
	religion = "court_of_adean"
	culture = "lorentish"
	father = 28 #King Rewan the Thorn of the West
	#mother = 6401 

	trait = greedy
	trait = zealous
	trait = content
	trait = education_stewardship_1

	trait = human


	994.11.1 = {
		birth = yes
	}

	1015.11.1 = {	
		death = {
			death_reason = death_murder
		}
	}
}

32 = {
	name = "Korvin"
	dynasty_house = house_lorentis
	religion = "court_of_adean"
	culture = "lorentish"
	father = 28 #King Rewan the Thorn of the West
	#mother = 6401 

	trait = ambitious
	trait = fickle
	trait = arrogant
	trait = education_learning_3
	trait = bastard

	trait = half_elf


	1002.4.20 = {
		birth = yes
	}

	1020.10.31 = { #Battle of Trialmount
		death = {	
			death_reason = death_battle
			killer = 33	#Sorcerer King
		}
	}
}

33 = {
	name = "Nichmer"
	religion = "castanorian_pantheon"
	culture = "corvurian"
	#father = 28 #King Rewan the Thorn of the West
	#mother = 6401 

	trait = ambitious
	trait = patient
	trait = paranoid
	trait = callous
	trait = education_learning_4

	trait = human

	994.11.1 = {
		birth = yes
	}

	1015.12.14 = {		#Battle of Balmire
		give_nickname = nick_the_sorcerer_king
	}

	1020.11.31 = {	#Battle of Trialmount
		death = {
			death_reason = death_battle
			killer = 34	#Clarimonde of Oldhaven
		}
	}
}

34 = {
	name = "Clarimonde"
	dynasty = 100 #of Oldhaven
	religion = "castanorian_pantheon"
	culture = "marcher"
	female = yes

	trait = brave
	trait = cynical
	trait = just
	trait = education_martial_3

	trait = human

	prowess = 13

	998.5.28 = {
		birth = yes
	}
}

35 = {
	name = "Chaderion"
	religion = "cult_of_the_dame"
	culture = "moon_elvish"

	trait = lustful
	trait = honest
	trait = compassionate
	trait = education_martial_3

	trait = fecund
	trait = physique_good_3

	trait = elf

	prowess = 10

	998.5.28 = {
		birth = yes
	}
}

36 = {
	name = "Ottrac"
	religion = "cult_of_the_dame"
	culture = "wexonard"

	trait = honest
	trait = humble
	trait = brave
	trait = education_martial_3

	trait = human

	prowess = 10


	988.2.25 = {
		birth = yes
	}

	1021.10.5 = {
		employer = 1	#Marion
		give_nickname = nick_the_wexonard
	}
}


37 = {
	name = "Sideric"
	religion = "cult_of_the_dame"
	culture = "damerian"	#CHANGE
	dynasty = dynasty_sidericis

	trait = patient
	trait = humble
	trait = vengeful
	trait = education_martial_4
	trait = cautious_leader

	trait = human

	prowess = 11

	992.3.23 = {
		birth = yes
	}

	1020.11.22 = {
		give_nickname = nick_the_patient
		add_matrilineal_spouse = 19	#Istralania Warsinger finally accepts Sideric's advances and she marries him after learning her priorities after Trialmount
		# set_relation_lover = character:19
	}
}


38 = {
	name = "Ricain"
	dynasty = dynasty_silistra	#Silistra
	religion = "cult_of_the_dame"
	culture = "damerian"	#CHANGE
	mother = 19 #Istralania Warsinger
	father = 37	#Sideric the Patient

	trait = rowdy
	trait = physique_good_3

	trait = half_elf

	1021.10.5 = {
		birth = yes
	}

}

39 = {	#not damerian loyalist
	name = "Vidraldus"
	dynasty = dynasty_acromis
	religion = "cult_of_the_dame"
	culture = "old_damerian"	
	father = 500 

	trait = deceitful
	trait = arrogant
	trait = ambitious

	trait = human

	978.4.1 = {
		birth = yes
	}

}

40 = {	#was a damerian loyalist
	name = "Acromar"
	dynasty = dynasty_acromis
	religion = "cult_of_the_dame"
	culture = "old_damerian"	
	father = 500 

	trait = brave
	trait = stubborn
	trait = content

	trait = human

	976.7.12 = {
		birth = yes
	}

	999.7.4 = {
		death = {	
			death_reason = death_battle
			killer = 33	#Sorcerer King
		}
	}

}

41 = {
	name = "Bolgrun"
	dna = 14_bolgrun_redstone
	dynasty = 16 #Redstone
	religion = "cult_of_the_dame"
	culture = "ruby_dwarvish"
	
	trait = education_martial_3
	trait = stubborn
	trait = patient
	trait = vengeful
	trait = strong
	trait = overseer
	trait = unyielding_defender
	
	898.1.1 = {
		birth = yes
	}
}

42 = {
	name = "Arnold"
	dynasty = 19 #Roysfort
	religion = "small_temple"
	culture = "roysfoot_halfling"
	
	995.1.1 = {
		birth = yes
	}
}

43 = {
	name = "Eustace"
	dna = 19_eustace_sil_uelaire
	dynasty = 18 # sil Uelaire
	religion = "cult_of_the_dame"
	culture = "damerian"
	
	trait = education_martial_3
	trait = reckless
	trait = ambitious
	trait = arrogant
	trait = greedy
	trait = human
	
	990.1.1 = {
		birth = yes
	}
}

44 = {
	name = "Barton"
	dynasty = 19 #Roysfort
	religion = "small_temple"
	culture = "roysfoot_halfling"
	
	trait = education_stewardship_3
	trait = paranoid
	trait = stubborn
	trait = zealous
	
	988.1.1 = {
		birth = yes
	}
}

45 = {
	name = "Jon"
	dynasty_house = house_appleseed #Appleseed
	religion = "small_temple"
	culture = "ciderfoot_halfling"
	
	997.4.5 = {
		birth = yes
	}
}

46 = {
	name = "Frederic"
	dynasty_house = house_peartree #Peartree
	religion = "small_temple"
	culture = "ciderfoot_halfling"
	
	995.3.4 = {
		birth = yes
	}
}

47 = {
	name = "Carwick"
	dna = 15_carwick_iochand
	dynasty = 20 #Iochand
	religion = "court_of_adean"
	culture = "creek_gnomish"
	
	trait = education_diplomacy_3
	trait = gregarious
	trait = impatient
	trait = lustful
	trait = shrewd
	
	997.3.2 = {
		birth = yes
	}
}

48 = {
	name = "Rocair"
	dna = 16_rocair_vernid
	dynasty = dynasty_vernid #Vernid
	religion = "court_of_adean"
	culture = "vernid"
	father = 513
	
	trait = education_martial_4
	trait = flexible_leader
	trait = brave
	trait = patient
	trait = just
	trait = strong
	trait = scarred
	trait = whole_of_body
	trait = gallant
	trait = human
	
	970.3.4 = {
		birth = yes
	}
}

49 = {
	name = "Daghr"
	dna = 18_daghr_askeling
	dynasty = 23 #Askeling
	religion = "castanorian_pantheon"
	culture = "reverian"
	
	984.4.5 = {
		birth = yes
	}
}

50 = {
	name = "Oddo"
	dynasty = 22 #Coddorran
	religion = "cult_of_the_dame"
	culture = "cliff_gnomish"
	
	993.2.7 = {
		birth = yes
	}
}

51 = {
	name = "Alenn"
	dynasty = dynasty_gawe #Gawe
	dna = 20_alenn_gawe_the_older
	religion = "cult_of_the_dame"
	culture = "gawedi"
	
	trait = education_martial_3
	trait = vengeful
	trait = wrathful
	trait = stubborn
	trait = irritable
	trait = physique_good_2
	trait = overseer
	
	972.4.5 = {
		birth = yes
	}
	
	1023.6.3 = {
		death = "1023.6.3"
	}
}

52 = {
	name = "Alenn"
	dynasty = dynasty_gawe #Gawe
	religion = "cult_of_the_dame"
	culture = "gawedi"
	father = 51 #Alenn Gawe
	
	991.4.5 = {
		birth = yes
	}
	
	1000.1.1 = {
		give_nickname = nick_the_younger
	}
	
	1014.7.6 = {
		death = "1014.7.6"
	}
}

53 = {
	name = "Godrac"
	dynasty = dynasty_gawe #Gawe
	religion = "cult_of_the_dame"
	culture = "gawedi"
	father = 51 #Alenn Gawe
	
	995.12.28 = {
		birth = yes
	}
	
	1000.1.1 = {
		give_nickname = nick_coldsteel
	}
	
	1015.7.14 = {
		death = "1015.7.14"
	}
}

54 = {
	name = "Ricard"
	dynasty = dynasty_gawe #Gawe
	religion = "cult_of_the_dame"
	culture = "gawedi"
	father = 51 #Alenn Gawe
	
	1001.3.4 = {
		birth = yes
	}
}

55 = {
	name = "Ulric"
	dynasty_house = house_coldsteel
	religion = "cult_of_the_dame"
	culture = "gawedi"
	father = 53 #Godrac Gawe
	
	1013.7.14 = {
		birth = yes
	}
}

56 = {
	name = "Warde"
	dynasty = 25 #Eaglecrest
	religion = "cult_of_the_dame"
	culture = "gawedi"
	
	997.3.4 = {
		birth = yes
	}
}

57 = {
	name = "Garthur"
	dynasty_house = house_beron
	religion = "cult_of_the_dame"
	culture = "moorman"
	
	972.4.7 = {
		birth = yes
	}
}

58 = {
	name = "Deris"
	dynasty_house = house_fouler
	religion = "cult_of_the_dame"
	culture = "moorman"
	
	1003.7.3 = {
		birth = yes
	}
}

59 = {
	name = "Marlen"
	dynasty_house = house_cottersea
	religion = "cult_of_the_dame"
	culture = "moorman"
	
	988.8.8 = {
		birth = yes
	}
}

60 = {
	name = "Ianren"
	dna = 22_ianren_the_rider
	dynasty = 27 #ta'Arthil
	religion = "cult_of_the_dame"
	culture = "moon_elvish"
	
	trait = elf
	
	712.11.17 = {	#second oldest famous elf, oldest is Garion
		birth = yes
	}
	
	1010.5.6 = {
		give_nickname = nick_the_rider
	}
}

61 = {
	name = "Maldorian"
	dna = 24_maldorian_the_navigator
	dynasty = 28 #ta'Galsheah
	religion = "cult_of_the_dame"
	culture = "moon_elvish"
	
	trait = elf
	
	908.5.4 = {
		birth = yes
	}
	
	950.3.2 = {
		give_nickname = nick_the_navigator
	}
}

62 = {
	name = "Rylen"
	dna = 21_rylen_adshaw
	dynasty = 29 #Adshaw
	religion = "cult_of_the_dame"
	culture = "old_alenic"
	
	980.4.6 = {
		birth = yes
	}
}

63 = {
	name = "Rikkard"
	dynasty = 30 #Serpentsgard
	religion = "cult_of_the_dame"
	culture = "blue_reachmen"
	
	975.4.5 = {
		birth = yes
	}
}

64 = {
	name = "Mason"
	dynasty = 31 #Cobbler
	religion = "cult_of_the_dame"
	culture = "blue_reachmen"
	
	965.7.6 = {
		birth = yes
	}
}

65 = {
	name = "Dyren"
	dynasty = 32 #Wex
	religion = "cult_of_the_dame"
	culture = "wexonard"
	
	1010.9.27 = {
		birth = yes
	}
}

66 = {
	name = "Rycroft"
	dynasty = 33 #Sugambic
	religion = "cult_of_the_dame"
	culture = "wexonard"
	
	956.4.6 = {
		birth = yes
	}
}

67 = {
	name = "Hargen"
	dynasty = 34 #Bisan
	religion = "cult_of_the_dame"
	culture = "wexonard"
	
	987.1.2 = {
		birth = yes
	}
}

68 = {
	name = "Dhaine"
	dynasty = 35 #Silverhammer
	religion = "cult_of_the_dame"
	culture = "silver_dwarvish"
	
	890.4.5 = {
		birth = yes
	}
}

69 = {
	name = "Laren"
	dynasty = 36 #Esmar
	religion = "cult_of_the_dame"
	culture = "esmari"
	
	987.4.7 = {
		birth = yes
	}
}

70 = {
	name = "Jaye"
	dynasty = 37 #Bennon
	religion = "cult_of_the_dame"
	culture = "esmari"
	
	997.6.5 = {
		birth = yes
	}
}

71 = {
	name = "Lindelin"
	dynasty = 38 #sil Estallen
	religion = "cult_of_the_dame"
	culture = "ryalani"
	female = yes
	
	trait = half_elf
	
	1003.6.11 = {
		birth = yes
	}
}

72 = {
	name = "Eren"
	dynasty = 39 #Ryalan
	religion = "cult_of_the_dame"
	culture = "ryalani"
	
	990.3.21 = {
		birth = yes
	}
}

73 = {
	name = "Leslin"
	dynasty = 40 #sil Leslinpár
	religion = "cult_of_the_dame"
	culture = "esmari"
	female = yes
	
	trait = half_elf
	
	1003.6.11 = {
		birth = yes
	}
}

74 = {
	name = "Arlen"
	dynasty = 41 #Havoran
	religion = "cult_of_the_dame"
	culture = "esmari"
	
	993.11.16 = {
		birth = yes
	}
}

75 = {
	name = "Ibenion"
	dna = 23_ibenion
	dynasty = 42 #ta'Luneteín
	religion = "cult_of_the_dame"
	culture = "moon_elvish"
	
	trait = elf
	
	728.12.8 = {
		birth = yes
	}
}

76 = {
	name = "Martin"
	dna = 25_martin_farran
	dynasty = 43 #Farran
	religion = "cult_of_the_dame"
	culture = "esmari"
	
	982.8.17 = {
		birth = yes
	}
}

77 = {
	name = "Damris"
	dynasty = 44 #Eldman
	religion = "cult_of_the_dame"
	culture = "crownsman"
	
	999.10.11 = {
		birth = yes
	}
}

78 = {
	name = "Harolt"
	dynasty = 45 #Vrorensson
	religion = "cult_of_the_dame"
	culture = "white_reachmen"
	
	984.11.12 = {
		birth = yes
	}
}

79 = {
	name = "Garrec"
	dna = 26_garrec_ebonfrost
	dynasty = 46 #Ebonfrost
	religion = "cult_of_the_dame"
	culture = "black_castanorian"
	
	997.8.29 = {
		birth = yes
	}
}

80 = {
	name = "Carl"
	dynasty_house = house_alcarsson #Alcarsson
	religion = "cult_of_the_dame"
	culture = "white_reachmen"
	
	990.12.9 = {
		birth = yes
	}
}

81 = {
	name = "Ivan"
	dynasty_house = house_alcarsson #Alcarsson
	religion = "cult_of_the_dame"
	culture = "white_reachmen"
	
	1002.8.25 = {
		birth = yes
	}
}

82 = {
	name = "Victor"
	religion = "cult_of_the_dame"
	culture = "white_reachmen"
	
	980.2.6 = {
		birth = yes
	}
}

83 = {
	name = "Wilford"
	religion = "cult_of_the_dame"
	culture = "white_reachmen"
	
	978.9.19 = {
		birth = yes
	}
}

84 = {
	name = "Marc"
	dynasty = 47 #Aldwoud
	religion = "cult_of_the_dame"
	culture = "white_reachmen"
	
	997.1.30 = {
		birth = yes
	}
}

85 = {
	name = "Allec"
	religion = "cult_of_the_dame"
	culture = "white_reachmen"
	
	982.4.7 = {
		birth = yes
	}
}

86 = {
	name = "Eric"
	religion = "cult_of_the_dame"
	culture = "white_reachmen"
	
	985.9.28 = {
		birth = yes
	}
}

87 = {
	name = "Maren"
	religion = "cult_of_the_dame"
	culture = "white_reachmen"
	
	972.7.18 = {
		birth = yes
	}
}

88 = {
	name = "Serondar"
	dynasty = 48 #Tederfremh
	religion = "cult_of_the_dame"
	culture = "moon_elvish"
	
	865.10.30 = {
		birth = yes
	}
}

89 = {
	name = "Wynstan"
	dynasty = 49 #sil Cast
	religion = "castanorian_pantheon"
	culture = "castanorian"
	
	987.3.17 = {
		birth = yes
	}
}

90 = {
	name = "Lain"
	dynasty = 50 #sil Anor
	religion = "castanorian_pantheon"
	culture = "castanorian"
	
	982.12.14 = {
		birth = yes
	}
}

91 = {
	name = "Hoger"
	dynasty = 51 #Marr
	religion = "castanorian_pantheon"
	culture = "marrodic"
	
	963.2.29 = {
		birth = yes
	}
}

92 = {
	name = "Chlothar"
	religion = "castanorian_pantheon"
	culture = "castanorian"
	
	971.3.12 = {
		birth = yes
	}
}

93 = {
	name = "Albert"
	dynasty = 52 #Balmire
	religion = "cult_of_the_dame"
	culture = "old_alenic"
	
	992.12.9 = {
		birth = yes
	}
}

94 = {
	name = "Urvik"
	dna = 27_urvik_ebonfrost
	dynasty = 46 #Ebonfrost
	religion = "castanorian_pantheon"
	culture = "black_castanorian"
	
	995.8.29 = {
		birth = yes
	}
}

6000000 = {	#Emperor Testor of the Testorian Empire
	name = "Testor"
	religion = "test_religion"
	culture = "damerian"
	1000.1.1 = {
		birth = yes
	}

}

#500 Jay Reserve
500 = {
	name = "Acromar"
	dynasty = dynasty_acromis
	religion = "cult_of_the_dame"
	culture = "old_damerian"	

	trait = zealous
	trait = calm
	trait = ambitious

	trait = human

	955.2.25 = {
		birth = yes
	}

	993.12.4 = {
		death = yes
	}

}

501 = {
	name = "Stefan"
	dynasty_house = house_saloren
	religion = "house_of_minara"
	culture = "roilsardi"	

	trait = gluttonous
	trait = craven
	trait = calm

	trait = human

	967.4.11 = {
		birth = yes
	}

	991.4.1 = {
		add_spouse = 512
	}
}

502 = {
	name = "Arman"
	dynasty_house = house_roilsard
	religion = "court_of_adean"
	culture = "roilsardi"	
	father = 6	#Gregoire, Count of Roilsard

	trait = human

	1001.5.16 = {
		birth = yes
	}
}

503 = {
	name = "Tomas"
	dynasty_house = house_roilsard
	religion = "court_of_adean"
	culture = "roilsardi"	
	father = 6	#Gregoire, Count of Roilsard

	trait = human

	1003.2.19 = {
		birth = yes
	}
}

504 = {
	name = "Arman"
	dynasty_house = house_roilsard
	religion = "court_of_adean"
	culture = "roilsardi"	

	trait = human

	956.11.4 = {
		birth = yes
	}

	1005.4.30 = {
		death = yes
	}
}

505 = {
	name = "Ruben"
	dynasty_house = house_roilsard
	religion = "court_of_adean"
	culture = "roilsardi"	
	father = 504

	trait = human

	984.3.1 = {
		birth = yes
	}

}

506 = {
	name = "Raisen"
	dynasty_house = house_loop #Loop
	religion = house_of_minara
	culture = "roilsardi"
	father = 5
	
	trait = education_diplomacy_3

	trait = human
	
	998.12.4 = {
		birth = yes
	}

}


507 = {
	name = "Silas"
	dynasty_house = house_vivin	#sil Vivin
	religion = "court_of_adean"
	culture = "roilsardi"
	father = 508

	trait = human
	
	1005.5.1 = {
		birth = yes
	}


}


508 = {
	name = "Marcel"
	dynasty_house = house_vivin	#sil Vivin
	religion = "court_of_adean"
	culture = "roilsardi"

	trait = human
	
	978.3.14 = {
		birth = yes
	}


	1001.1.12 = {	#mass execution
		death = {
			death_reason = death_execution
		}
	}

}

509 = {
	name = "Loui"
	dynasty_house = house_loop #Loop
	religion = house_of_minara
	culture = "roilsardi"
	father = 5

	trait = human
	
	1001.5.15 = {
		birth = yes
	}

}

510 = {
	name = "Lilith"
	dynasty = dynasty_sorncost
	religion = court_of_adean
	culture = "sorncosti"
	female = yes
	father = 511

	trait = ambitious
	trait = deceitful
	trait = lustful

	trait = human
	
	998.5.15 = {
		birth = yes
	}

}

511 = {
	name = "Petran"
	dynasty = dynasty_sorncost
	religion = court_of_adean
	culture = "sorncosti"

	trait = zealous
	trait = wrathful
	trait = sadistic
	trait = human
	
	973.2.2 = {
		birth = yes
	}

	1019.2.5 = { # Murdered by Alcuin, who wanted to be ruler to control elven narrative
		death = {	
			death_reason = death_murder
			killer = 8
		}
	}

}

512 = {
	name = "Petrona"
	dynasty_house = house_vivin
	religion = "house_of_minara"
	culture = "roilsardi"	
	female = yes

	trait = human

	974.3.28 = {
		birth = yes
	}
}

513 = {
	name = "Arman"	#Rocair of Verne's dad
	dynasty = dynasty_vernid
	religion = "cult_of_the_dame"
	culture = "vernid"	
	trait = education_martial_4

	trait = human

	950.6.2 = {
		birth = yes
	}

	989.6.30 = { # Slaughter of Cronesford, during Invasion of East Dameshead
		death = {	
			death_reason = death_battle
			killer = 514	#Iacob the Betrayer
		}
	}
}


514 = {
	name = "Iacob"	#Iacob the Betrayer, acolyte of the Sorcerer King
	dynasty = dynasty_iacoban
	religion = "castanorian_pantheon"
	culture = "corvurian"	
	trait = education_diplomat_4

	trait = human

	961.4.18 = {
		birth = yes
	}
	1015.11.1 = {
		give_nickname = nick_the_betrayer
	}

}

515 = {
	name = "Alec"
	dynasty = 9	#Pearlman
	religion = "castanorian_pantheon"
	culture = "pearlsedger"
	father = 3	#Aron Pearlman

	999.9.1 = {
		birth = yes
	}
	1017.3.5 = {
		add_spouse = 517	#Matilda of Lanpool
	}
}

516 = {
	name = "Haakon"
	dynasty = 9	#Pearlman
	religion = "castanorian_pantheon"
	culture = "pearlsedger"
	father = 515	#Alec Pearlman
	mother = 517	#Matilda of Lanpool

	1021.2.5 = {
		birth = yes
	}
}

517 = {
	name = "Matilda"
	dynasty_house = house_lanpool
	religion = "castanorian_pantheon"
	culture = "tretunic"
	female = yes

	997.7.7 = {
		birth = yes
	}
}